# Deploy slides

```console
> docker run -d --name edu-se-am -p 8000:1948 --restart unless-stopped -v $PWD/slides:/slides webpronl/reveal-md:latest
```

```text
server {
    listen 80;

        server_name se-am.edu.swampbuds.me;

        location / {
                proxy_set_header   X-Forwarded-For $remote_addr;
                proxy_set_header   Host $http_host;
                proxy_pass         "http://127.0.0.1:8000";
                client_max_body_size 200M;
    }
}
```

```text
systemctl restart nginx.service
```
