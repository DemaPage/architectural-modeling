# Системная инженерия. <br/> Архитектурное моделирование компьютерных систем

## Лекция 7

### Архитектурные стили

Пенской А.В., 2022

----

### План лекции

- Architectural Views
- Architectural Styles
    - Module Styles
    - Component-and-Connector Styles
    - Allocation Styles

---

## Notations for Architecture Views

<div class="row"><div class="col">

1. **Informal notations**. The views are depicted (often graphically) using general-purpose diagramming. The semantics are characterized in natural language and cannot be formally analyzed.
2. **Semiformal notations**. Views are expressed in a standardized notation that prescribes graphical elements and rules of construction, but does not provide a complete semantic treatment of the meaning of those elements (e.g. UML).
3. **Formal notations**. The views are described in a notation that has a precise (usually mathematically based) semantics (ADLs).

</div><div class="col">

![](figures/meme-architecture-notation.jpg) <!-- .element: class="fullscreen" -->

</div></div>

---

## Классы архитектурных стилей

<div class="row"><div class="col">

1. Module Styles.
2. Component-and-Connector Styles.
3. Allocation Styles.

![](figures/arch-model-component.png)

</div><div class="col">

![](figures/arch-style-allocation.png)

</div></div>

----

### Modules vs. Components

![](figures/module-vs-component-in-detail.png) <!-- .element: class="fullscreen" -->

---

## Module Styles

![](figures/style-model.png) <!-- .element: class="fullscreen" -->

----

### Decomposition Style

![](figures/style-decomposition.png) <!-- .element: class="fullscreen" -->

----

<div class="row"><div class="col">

![](figures/style-decomposition-simple.png) <!-- .element: class="fullscreen" -->

</div><div class="col">

![](figures/style-decomposition-complex.png) <!-- .element: class="fullscreen" -->

</div></div>

----

### Uses Style

![](figures/style-uses.png) <!-- .element: class="fullscreen" -->

----

<div class="row"><div class="col">

![](figures/style-uses-simple.png) <!-- .element: class="fullscreen" -->

</div><div class="col">

![](figures/style-uses-complex.png) <!-- .element: class="fullscreen" -->

</div></div>

----

<div class="row"><div class="col">

![](figures/style-uses-table-1.png)  <!-- .element: class="fullscreen" -->

</div><div class="col">

![](figures/style-uses-table-2.png)  <!-- .element: class="fullscreen" -->

</div></div>

----

### Generalisation Style

![](figures/style-generalization.png)  <!-- .element: class="fullscreen" -->

----

<div class="row"><div class="col">

![](figures/style-generalization-1.png)  <!-- .element: class="fullscreen" -->

![](figures/style-generalization-2.png)  <!-- .element: class="fullscreen" -->

</div><div class="col">

![](figures/style-generalization-3.png)  <!-- .element: class="fullscreen" -->

</div></div>

----

### Layered Style

![](figures/style-layered.png)  <!-- .element: class="fullscreen" -->

----

<div class="row"><div class="col">

![](figures/style-layered-1.png)  <!-- .element: class="fullscreen" -->

![](figures/style-layered-2.png)  <!-- .element: class="fullscreen" -->

</div><div class="col">

![](figures/style-layered-3.png) <!-- .element: height="250px" -->

![](figures/style-layered-4.png) <!-- .element: height="250px" -->

</div></div>

----

### Aspect Style

![](figures/style-aspect.png)

----

<div class="row"><div class="col">

![](figures/style-aspect-1.png)

</div><div class="col">

![](figures/style-aspect-2.png)

</div></div>

----

### Data Model

![](figures/style-data-model.png)

----

![](figures/style-data-model-1.png) <!-- .element: height="250px" -->

![](figures/style-data-model-2.png) <!-- .element: height="350px" -->

---

## Component-and-Connector Styles

![](figures/style-component.png)

----

### Data Flow Style

![](figures/style-data-flow.png) <!-- .element: class="fullscreen" max-height="50%" -->

----

![](figures/style-data-flow-2.png) <!-- .element: class="fullscreen" -->

----

![](figures/style-data-flow-1.png) <!-- .element: class="fullscreen" -->

----

### Call-Return Style. Peer-to-Peer Style

![](figures/style-call-return.png) <!-- .element: class="fullscreen" -->

----

![](figures/style-call-return-1.png) <!-- .element: class="fullscreen" -->

----

![](figures/style-call-return-2.png)

----

### Event-Based Style. Publish-Subscribe Style

![](figures/style-event-based.png) <!-- .element: class="fullscreen" -->

----

### Repository Style

![](figures/style-repository.png) <!-- .element: class="fullscreen" -->

----

![](figures/style-repository-1.png) <!-- .element: class="fullscreen" -->

----

### Hybrid

![](figures/style-hybrid.png) <!-- .element: class="fullscreen" -->

---

## Allocation Styles

![](figures/style-allocation.png)

----

### Deployment Style

![](figures/style-deployment.png)

----

![](figures/style-deployment-1.png) <!-- .element: class="fullscreen" -->

----

### Install Style

![](figures/style-install.png)

----

![](figures/style-install-1.png) <!-- .element: class="fullscreen" -->

----

### Work Assignment Style

![](figures/style-assignment.png)

----

![](figures/style-assignment-1.png) <!-- .element: class="fullscreen" -->

---

## Выбор архитектурного представления

### Этап проекта

![](figures/style-specialization-due-lifecycle.png)

----

### Stakeholder

![](figures/arch-view-selection.png) <!-- .element: class="fullscreen" -->
